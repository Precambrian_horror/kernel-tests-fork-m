RHEL-7 kselftest backports
==========================

For RHEL-7, there are essentially two backporting contexts to target:

  1) kernel-3.10.0-1062.el7 to kernel-3.10.0-1127, this includes only
     the original introduction of the livepatching kselftests

  2) kernel-3.10.0-1128.el7 onward, which includes kernel updates for
     the PERMANENT ftrace_ops flag and corresponding livepatching
     kselftests

These correspond to (1) just after RHEL-7.7 GA and (2) just after
RHEL-7.8 GA releases.


Timeline
--------

An annotated git log timeline (in chronological order) of releases and
interesting rhel7.git commits:

  A [redhat] kernel-3.10.0-1062.el7
    ...
    [lib] selftests/livepatch: introduce tests
    [kernel] livepatch: Send a fake signal periodically
    [kernel] livepatch: Remove signal sysfs attribute
    [samples] livepatch: samples: non static warnings fix
    [kernel] livepatch: core: Return EOPNOTSUPP instead of ENOSYS
    [kernel] livepatch: Introduce klp_for_each_patch macro
    [lib] livepatch: return -ENOMEM on ptr_id() allocation failure
    [lib] livepatch: Proper error handling in the shadow variables selftest
    [kernel] livepatch: Module coming and going callbacks can proceed with all listed patches
    [tools] livepatch/selftests: use "$@" to preserve argument list
    [tools] selftests/livepatch: use TEST_PROGS for test scripts
    [tools] selftests/livepatch: Add functions.sh to TEST_PROGS_EXTENDED
  B [redhat] kernel-3.10.0-1067.el7
    ...
  C [redhat] kernel-3.10.0-1127
    [tools] selftests/livepatch: push and pop dynamic debug config
    [kernel] ftrace: Introduce PERMANENT ftrace_ops flag
    [tools] selftests/livepatch: Make dynamic debug setup and restore generic
    [tools] selftests/livepatch: Test interaction with ftrace_enabled
  D [redhat] kernel-3.10.0-1128.el7
    ...
  E [redhat] kernel-3.10.0-1159.el7
  

Key
---
  
  A = kernel-3.10.0-1062.el7, RHEL-7.7 released
  B = kernel-3.10.0-1067.el7, livepatching kselftests introduced
  C = kernel-3.10.0-1127.el7, RHEL-7.8 released
  D = kernel-3.10.0-1128.el7, kernel changes to support PERMANENT
      ftrace_ops flag, tests updated accordingly
  E = kernel-3.10.0-1159.el7, RHEL-7.9 released

Unfortunately the changes by D modify the kselftests enough that the
same set of patches do not apply to backporting context (1) and (2).

The simple algorithm used by run_tests.sh is to find the latest
backports/<KVER> directory that is earlier than the running kernel, then
only apply the patches found in that directory.
