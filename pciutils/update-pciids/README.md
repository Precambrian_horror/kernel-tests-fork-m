# PCI Sanity Smoke test.
Run pciids update, and check new pciids updated successfully.

## How to run it

### Dependencies
Please refer to the top-leve README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
